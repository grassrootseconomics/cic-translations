# standard imports
import logging

# external imports
import yamale

# local imports

logg = logging.getLogger(__file__)


def validate_translation_file(schema_file_name: str, schema_file_path: str, translation_file: str):
    """This function validates a yml file against a pre-defined schema.
    :param schema_file_name: The specific schema file name against which the translation file should be validated.
    System currently supports. [helpers, sms and ussd *.schema.yml]
    :type schema_file_name: str
    :param schema_file_path: Path to schema files.
    :type schema_file_path: str
    :param translation_file: The translation file being checked for validity.
    :type translation_file: str
    """
    schema = yamale.make_schema(path=f'{schema_file_path}/{schema_file_name}.schema.yml')
    logg.debug(f'Validating: {translation_file} against: {schema.name}.')
    data = yamale.make_data(translation_file)
    yamale.validate(schema, data)
