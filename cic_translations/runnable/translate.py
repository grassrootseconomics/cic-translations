# standard imports
import argparse
import logging
import os

# external imports

# local imports
from cic_translations.processor import generate_translation_files, parse_csv


# define a logging system
logging.basicConfig(level=logging.WARNING)
logg = logging.getLogger()

arg_parser = argparse.ArgumentParser(description='CLI for parsing translation files from CSV to yaml.')
arg_parser.add_argument('-f', type=str, help='The CSV file from which to generate the YAML translation files.')
arg_parser.add_argument('-n', type=str, help='Name of the resultant file with translations.')
arg_parser.add_argument('-o', default='out', type=str, help='Output directory for translation files generation.')
arg_parser.add_argument('-v', help='be verbose', action='store_true')
arg_parser.add_argument('-vv', help='be more verbose', action='store_true')
args = arg_parser.parse_args()


# process out directory
if not os.path.exists(args.o):
    logg.debug(f'Output directory not found. Creating: {args.o}')
    os.makedirs(args.o, exist_ok=True)

# define log levels
if args.vv:
    logging.getLogger().setLevel(logging.DEBUG)
elif args.v:
    logging.getLogger().setLevel(logging.INFO)


if __name__ == '__main__':
    parsed_csv = parse_csv(file_path=args.f)
    parent_dir = os.path.dirname(__file__)
    schema_dir = os.path.join(parent_dir, '..', 'data', 'schema')
    generate_translation_files(
        parsed_csv=parsed_csv, schema_file_path=schema_dir, translation_file_path=args.o, translation_file_type=args.n)
