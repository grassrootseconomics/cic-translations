# standard imports

import csv
import logging
import os
from functools import reduce

# external imports
import yaml

# local imports
from cic_translations.validator import validate_translation_file

logg = logging.getLogger(__file__)


def get_target(dictionary: dict, keys: list):
    """This function takes a list and creates a nested dictionary with each element in the list being used as a key
    to the subsequent nested element until the list is reduced to one element.
    :param dictionary: The resultant dictionary.
    :type dictionary: dict
    :param keys: A list to form nested keys in the resultant dictionary.
    :type keys: dict
    :return: A generator object containing reduced list into dictionary.
    :rtype: iterable
    """
    return reduce(lambda d, k: d.setdefault(k, {}), keys, dictionary)


def generate_translation_files(parsed_csv: list,
                               schema_file_path: str,
                               translation_file_path: str,
                               translation_file_type: str):
    """This function creates translation files from parsing a CSV that contains translation file keys and corresponding
    columns with language translations.
    :param parsed_csv: A csv converted into a list containing lists.
    :type parsed_csv: list
    :param schema_file_path: Path to schema files.
    :type schema_file_path: str
    :param translation_file_path: Path to which resultant translation files are written.
    :type translation_file_path:  str
    :param translation_file_type: The nature of the translation file, system currently supports helper, sms and ussd.
    :type translation_file_type: str
    """
    languages = parsed_csv[0][1:]
    translations = parsed_csv[1:]

    for i in range(len(languages)):
        translation_object = {}
        result = {}

        for translation in translations:
            translation_keys = translation[0].split('.')
            set_target(result, translation_keys, translation[i + 1])

        translation_object = {**translation_object, **result}
        language = languages[i]
        data = {
            language: translation_object
        }
        translation_file_name = f'{translation_file_type}.{language}.yml'
        translation_file = os.path.join(translation_file_path, translation_file_name)
        write_translation_file(translation_file=translation_file, translation_object=data)
        validate_translation_file(
            schema_file_name=translation_file_type,
            schema_file_path=schema_file_path,
            translation_file=translation_file)


def parse_csv(file_path: str) -> list:
    """This function parses a csv into a list.
    :param file_path: Path to csv file.
    :type file_path: str
    :return: List containing all the rows of the csv.
    :rtype: list
    """
    logg.debug(f'Reading from file: {file_path}.')
    with open(file_path, "r") as translation_file:
        reader = csv.reader(translation_file)
        return [list(row) for row in reader]


def set_target(dictionary, keys, value):
    """
    :param dictionary:
    :type dictionary:
    :param keys:
    :type keys:
    :param value:
    :type value:
    :return:
    :rtype:
    """
    parent = get_target(dictionary, keys[:-1])
    parent[keys[-1]] = value


def write_translation_file(translation_file: str, translation_object: dict):
    """
    :param translation_file:
    :type translation_file:
    :param translation_object:
    :type translation_object:
    :return:
    :rtype:
    """
    with open(translation_file, "w") as translation_file:
        logg.debug(f'Writing to translation file: {translation_file.name}.')
        yaml.dump(translation_object, translation_file, default_style="|", explicit_end=True, explicit_start=True)
